This is A2019 package to convert character code of text files.
This package supports Shift-JIS / UTF-8 / EUC-JP character code.

Takehiko Isono